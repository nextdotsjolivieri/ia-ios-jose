//
//  SQLiteDataManager.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/2/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import Foundation
import SQLite

class SQLiteDataManager {
    static let shared = SQLiteDataManager()
    
    private final var connection: Connection? = nil
    
    final var database: CinePolisDatabase? = nil {
        didSet {
            guard let database = database else { return }
            
            do {
                var documentsURL = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0]
                documentsURL.appendPathComponent(database.filePath)
                
                connection = try Connection(documentsURL.absoluteString, readonly: true)
            } catch let error as NSError {
                print("Database error", error)
            }
        }
    }
    
    func getComplexesAsArray() -> Array<Row> {
        var complexesArray = Array<Row>()
        
        guard let connection = connection else {
            return complexesArray
        }
        
        do {
            let complexTable = Table(ComplexHelper.TABLE_NAME)
            let query = complexTable.select(ComplexHelper.id,
                                            ComplexHelper.name,
                                            ComplexHelper.address,
                                            ComplexHelper.phone,
                                            ComplexHelper.latitude,
                                            ComplexHelper.longitude)
                        .order(ComplexHelper.name)
            complexesArray = Array<Row>(try connection.prepare(query))
        } catch let error as NSError {
            print("Database error", error)
        }
        
        return complexesArray
    }
    
    func getMoviesAsArray(by complex: Row) -> Array<Row> {
        var moviesArray = Array<Row>()
        
        guard let connection = connection else {
            return moviesArray
        }
        
        do {
            let movieTable = Table(MovieHelper.TABLE_NAME)
            let complexTable = Table(ComplexHelper.TABLE_NAME)
            let billboardTable = Table(BillboardHelper.TABLE_NAME)
            
            let query = movieTable.join(billboardTable, on: movieTable[MovieHelper.id] == BillboardHelper.idMovie)
                .join(complexTable, on: complexTable[ComplexHelper.id] == BillboardHelper.idComplex)
                .filter(complexTable[ComplexHelper.id] == complex[ComplexHelper.id])
                .select(movieTable[MovieHelper.id].distinct,
                        movieTable[MovieHelper.title],
                        movieTable[MovieHelper.genre],
                        movieTable[MovieHelper.classification],
                        movieTable[MovieHelper.duration],
                        movieTable[MovieHelper.director],
                        movieTable[MovieHelper.actors],
                        movieTable[MovieHelper.synopsis])
            .order(movieTable[MovieHelper.title])
            
            moviesArray = Array<Row>(try connection.prepare(query))
        } catch let error as NSError {
            print("Database error", error)
        }
        
        return moviesArray
    }
    
    func getMultimediaAsArray(by movie: Row) -> Array<Row> {
        var multimediaArray = Array<Row>()
        
        guard let connection = connection else {
            return multimediaArray
        }
        
        do {
            let movieTable = Table(MovieHelper.TABLE_NAME)
            let multimediaTable = Table(MultimediaHelper.TABLE_NAME)
            
            let query = multimediaTable.join(movieTable, on: movieTable[MovieHelper.id] == MultimediaHelper.idMovie)
                .filter(multimediaTable[MultimediaHelper.idMovie] == movie[MovieHelper.id] &&
                    multimediaTable[MultimediaHelper.type] == "Imagen")
                .select(multimediaTable[MultimediaHelper.file])

            multimediaArray = Array<Row>(try connection.prepare(query))
        } catch let error as NSError {
            print("Database error", error)
        }
        
        return multimediaArray
    }
    
    func getBillboardAsArray(by movie: Row, and complex: Row) -> Array<Row> {
        var billboardArray = Array<Row>()
        
        guard let connection = connection else {
            return billboardArray
        }
        
        do {
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let dateString = formatter.string(from: date)
            
            
            let movieTable = Table(MovieHelper.TABLE_NAME)
            let complexTable = Table(ComplexHelper.TABLE_NAME)
            let billboardTable = Table(BillboardHelper.TABLE_NAME)
            
            let query = billboardTable.join(movieTable, on: movieTable[MovieHelper.id] == BillboardHelper.idMovie)
                .join(complexTable, on: complexTable[ComplexHelper.id] == BillboardHelper.idComplex)
                .filter(billboardTable[BillboardHelper.idComplex] == complex[ComplexHelper.id] &&
                    billboardTable[BillboardHelper.idMovie] == movie[MovieHelper.id] &&
                    billboardTable[BillboardHelper.date] == dateString)
                .select(billboardTable[BillboardHelper.schedule],
                        billboardTable[BillboardHelper.date],
                        billboardTable[BillboardHelper.cinema])
                .order(billboardTable[BillboardHelper.schedule])
            
            billboardArray = Array<Row>(try connection.prepare(query))
        } catch let error as NSError {
            print("Database error", error)
        }
        
        return billboardArray
    }
}
