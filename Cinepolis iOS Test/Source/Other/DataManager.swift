//
//  DataManager.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/1/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import RealmSwift
import AlamofireObjectMapper
import SVProgressHUD

class DataManager {
    
    static func fetchData <T: Object> (
        method: HTTPMethod = .get,
        url : String,
        type: T.Type,
        success: @escaping (_ result: [T]) -> Void,
        fail: @escaping (_ error: NSError) -> Void)
        -> Void where T: Mappable {
            Alamofire.request(url, method: method, parameters: nil, encoding: JSONEncoding.default).responseArray { ( response : DataResponse <[T]>) in
                switch response.result {
                case .success:
                    guard let value = response.result.value else {
                        fail(NSError(domain: url, code: (response.response?.statusCode)!, userInfo: [:]))
                        return
                    }
                    success(value)
                case .failure(let error):
                    fail(error as NSError)
                }
            }
    }
    
    static func fetchFile(showProgress: Bool = true, urlFile: String, success: @escaping (_ destinationUrl: URL) -> Void) {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)[0]
            let pathComponent = UUID().uuidString + kDatabaseFileExtension
            documentsURL.appendPathComponent(pathComponent)
            return (documentsURL, [.removePreviousFile])
        }
        
//        let utilityQueue = DispatchQueue.global(qos: .utility)
        
        Alamofire.download(urlFile, to: destination)
            /*.downloadProgress(queue: utilityQueue, closure: { (progress) in
            if showProgress {
                DispatchQueue.main.async {
                    SVProgressHUD.showProgress(Float(progress.fractionCompleted))
                }
            }
        })
 */.responseData { response in
            if let destinationUrl = response.destinationURL {
                success(destinationUrl)
            }
        }
    }
}
