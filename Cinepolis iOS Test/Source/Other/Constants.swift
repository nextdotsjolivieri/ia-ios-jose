//
//  Constants.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/1/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import Foundation

let kCitiesURL = "http://api.cinepolis.com.mx/Consumo.svc/json/ObtenerCiudades"
let kDatabaseURL = "http://api.cinepolis.com.mx/sqlite.aspx?idCiudad="
let kDatabaseFileExtension = ".sqlite"
let kImageBaseURL = "http://www.cinepolis.com/_MOVIL/iPhone/galeria/thumb/"
let kVideoBaseURL = "http://videos.cinepolis.com/\(kVideoPlaceholder)/1/2/\(kVideoPlaceholder).mp4"
let kVideoPlaceholder = "MOVIE_ID"

let kLoadingTitle = "Loading.."
let kDownloadingTitle = "Downloading.."
let kCinemaTitle = "Cinema"

let kShareTitle = "Share Schedule"
let kShareMessage = "Which method would you like to use?"
let kTwitter = "Twitter"
let kFacebook = "Facebook"
let kCancelTitle = "Cancel"

let kOkTitle = "OK"
let kLoginToShareTitle = "Please login in order to share."
