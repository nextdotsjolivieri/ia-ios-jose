//
//  MovieTableViewCell.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/3/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var classificationLabel: UILabel!
    
    func fillData(movieTitle: String?, movieGenre: String?, movieClassification: String?) {
        titleLabel.text = movieTitle
        genreLabel.text = movieGenre
        classificationLabel.text = movieClassification
    }
}
