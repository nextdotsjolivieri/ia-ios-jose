//
//  MultimediaImageCollectionViewCell.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/3/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import UIKit
import Kingfisher

class MultimediaImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    func fillData(imageName: String?){
        guard let imageName = imageName else { return }
        let imageURL = URL(string: kImageBaseURL + imageName)
        imageView.kf.setImage(with: imageURL)
    }
}
