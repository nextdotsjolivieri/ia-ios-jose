//
//  MovieScheduleTableViewCell.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/3/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import UIKit

protocol MovieScheduleTableViewCellDelegate: class {
    func didSelectShare(cell: MovieScheduleTableViewCell)
}

class MovieScheduleTableViewCell: UITableViewCell {

    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var cinemaLabel: UILabel!
    
    weak var delegate: MovieScheduleTableViewCellDelegate? = nil
    
    func fillData(schedule: String?, date: String?, cinema: Int64?){
        scheduleLabel.text = schedule
        dateLabel.text = date
        if let cinema = cinema {
            cinemaLabel.text = kCinemaTitle + " \(cinema)"
        }
    }
    
    @IBAction func shareButtonTapped(_ sender: UIButton) {
        delegate?.didSelectShare(cell: self)
    }
}
