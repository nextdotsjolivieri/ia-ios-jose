//
//  FieldValueTableViewCell.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/1/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import UIKit

class FieldValueTableViewCell: UITableViewCell {

    @IBOutlet private weak var fieldLabel: UILabel!
    @IBOutlet private weak var valueLabel: UILabel!
    
    func fillData(field: String?, value: String?){
        fieldLabel.text = field
        valueLabel.text = value
    }
}
