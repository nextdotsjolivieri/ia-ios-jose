//
//  MultimediaHelper.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/3/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import Foundation
import SQLite

class MultimediaHelper {
    static let TABLE_NAME = "Multimedia"
    static let id = Expression<Int64>("Id")
    static let idMovie = Expression<Int64>("IdPelicula")
    static let file = Expression<String?>("Archivo")
    static let type = Expression<String?>("Tipo")
}
