//
//  ComplexHelper.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/2/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import Foundation
import SQLite

class ComplexHelper {
    static let TABLE_NAME = "Complejo"
    static let id = Expression<Int64>("Id")
    static let name = Expression<String?>("Nombre")
    static let address = Expression<String?>("Direccion")
    static let phone = Expression<String?>("Telefono")
    static let latitude = Expression<String?>("Latitud")
    static let longitude = Expression<String?>("Longitud")
}
