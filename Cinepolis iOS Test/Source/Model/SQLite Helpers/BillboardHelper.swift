//
//  BillboardHelper.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/3/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import Foundation
import SQLite

class BillboardHelper {
    static let TABLE_NAME = "Cartelera"
    static let idComplex = Expression<Int64>("IdComplejo")
    static let idMovie = Expression<Int64>("IdPelicula")
    static let schedule = Expression<String?>("Horario")
    static let date = Expression<String?>("Fecha")
    static let cinema = Expression<Int64?>("Sala")
    
    static func toString(billboard: Row) -> String {
        var billboardString = ""
        
        guard let date = billboard[BillboardHelper.date],
            let schedule = billboard[BillboardHelper.schedule],
            let cinema = billboard[BillboardHelper.cinema] else {
                return billboardString
        }
        
        billboardString += "Date: \(date)\n"
        billboardString += "Schedule: \(schedule)\n"
        billboardString += "Cinema: \(cinema)"
        
        return billboardString
    }
}
