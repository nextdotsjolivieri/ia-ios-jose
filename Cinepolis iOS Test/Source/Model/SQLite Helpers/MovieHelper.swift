//
//  MovieHelper.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/3/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import Foundation
import SQLite

class MovieHelper {
    static let TABLE_NAME = "Pelicula"
    static let id = Expression<Int64>("Id")
    static let title = Expression<String?>("Titulo")
    static let genre = Expression<String?>("Genero")
    static let classification = Expression<String?>("Clasificacion")
    static let duration = Expression<String?>("Duracion")
    static let director = Expression<String?>("Director")
    static let actors = Expression<String?>("Actores")
    static let synopsis = Expression<String?>("Sinopsis")
}
