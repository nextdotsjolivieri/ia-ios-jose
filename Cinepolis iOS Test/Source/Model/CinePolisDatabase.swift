//
//  CinePolisDatabase.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/1/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class CinePolisDatabase: Object {
    
    dynamic var filePath = ""
    dynamic var city: City?
    
    override static func primaryKey() -> String? {
        return "filePath"
    }
    
    convenience init(filePath: String, city: City?){
        self.init()
        self.filePath = filePath
        self.city = city
    }
}
