//
//  ComplexPointAnnotation.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/2/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import UIKit
import MapKit
import SQLite

class ComplexPointAnnotation: MKPointAnnotation {
     var complex: Row?
    
    convenience init(complex: Row){
        self.init()
        self.complex = complex
    }
}
