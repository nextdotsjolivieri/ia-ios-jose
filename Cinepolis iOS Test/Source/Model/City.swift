//
//  City.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/1/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class City: Object, Mappable {
    
    dynamic var id = 0
    dynamic var idCountry = 0
    dynamic var idState = 0
    dynamic var latitude = 0.0
    dynamic var longitude = 0.0
    dynamic var name = ""
    dynamic var country = ""
    dynamic var state = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id    <- map["Id"]
        idCountry <- map["IdPais"]
        idState <- map["IdEstado"]
        latitude <- map["Latitud"]
        longitude <- map["Longitud"]
        name <- map["Nombre"]
        country <- map["Pais"]
        state <- map["Estado"]
    }
}
