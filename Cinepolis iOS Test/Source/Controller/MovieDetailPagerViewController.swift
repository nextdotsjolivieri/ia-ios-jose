//
//  MovieDetailPagerViewController.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/3/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import UIKit
import SQLite
import XLPagerTabStrip
protocol MovieDetailTableViewControllerDelegate {
    func showImagesFromGalley(images:[Row], ind:Int)
}


class MovieDetailPagerViewController: ButtonBarPagerTabStripViewController {
    
    var movie: Row? = nil
    var complex: Row? = nil
    final let segueIdentifier_presentImage = "presentImages"

    override func viewDidLoad() {
        configureStyle()
        super.viewDidLoad()
    }
    
    private func configureStyle(){
        settings.style.buttonBarBackgroundColor = .gray
        settings.style.buttonBarItemBackgroundColor = .gray
        settings.style.selectedBarBackgroundColor = .lightGray
    }
 
    override public func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        guard let movie = movie, let storyboard = storyboard else {
            return []
        }
        
        let movieGalleryCollectionViewController = storyboard.instantiateViewController(withIdentifier:
            "MovieGalleryCollectionViewController") as! MovieGalleryCollectionViewController
        
        movieGalleryCollectionViewController.movie = movie
        movieGalleryCollectionViewController.delegate = self
        
        let movieTrailerViewController = storyboard.instantiateViewController(withIdentifier:
            "MovieTrailerViewController") as! MovieTrailerViewController
        
        movieTrailerViewController.movie = movie
        
        let movieSchedulesTableViewController = storyboard.instantiateViewController(withIdentifier:
            "MovieSchedulesTableViewController") as! MovieSchedulesTableViewController
        
        movieSchedulesTableViewController.movie = movie
        movieSchedulesTableViewController.complex = complex
        
        let movieDetailTableViewController = storyboard.instantiateViewController(withIdentifier:
            "MovieDetailTableViewController") as! MovieDetailTableViewController
        
        movieDetailTableViewController.movie = movie
        
        return [movieDetailTableViewController, movieGalleryCollectionViewController, movieTrailerViewController, movieSchedulesTableViewController]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifier_presentImage {
            if let destinationVC = segue.destination as? MovieImageFromGalleryViewController {
                guard let data = sender as? [String : Any] else {
                    return
                }
                destinationVC.multimediaArray = data["images"] as! [Row]
                destinationVC.selected = data["indSelected"] as! Int
            }
        }
    }
}

extension MovieDetailPagerViewController: MovieDetailTableViewControllerDelegate{
    
    func showImagesFromGalley(images: [Row], ind: Int) {
        let dataSender = ["images": images, "indSelected": ind] as [String : Any]
        performSegue(withIdentifier: segueIdentifier_presentImage, sender: dataSender)
    }
}

