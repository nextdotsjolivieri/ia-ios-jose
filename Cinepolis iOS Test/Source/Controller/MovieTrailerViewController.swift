//
//  MovieTrailerViewController.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/3/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//
import UIKit
import SQLite
import XLPagerTabStrip
import AVKit
import AVFoundation

class MovieTrailerViewController: AVPlayerViewController {
    
    fileprivate final let itemInfo: IndicatorInfo = "Trailer"
    var movie: Row? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.player?.play()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.player?.pause()
    }
}

// MARK: - IndicatorInfoProvider

extension MovieTrailerViewController: IndicatorInfoProvider{
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

fileprivate extension MovieTrailerViewController {
    func loadData(){
        guard let movie = movie else { return }
        let idMovie = "\(movie[MovieHelper.id])"
        
        let urlVideo = kVideoBaseURL.replacingOccurrences(of: kVideoPlaceholder, with: idMovie)
        if let url = URL(string: urlVideo) {
            configurePlayer(with: url)
        }
    }
    
    func configurePlayer(with url: URL){
        let player = AVPlayer(url: url)
        self.player = player
        self.player?.seek(to: kCMTimeZero)
    }
}
