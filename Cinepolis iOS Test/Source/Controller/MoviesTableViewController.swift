//
//  MoviesTableViewController.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/3/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import UIKit
import SQLite

class MoviesTableViewController: UITableViewController {
    
    private final let segueIdentifier_presentMovieDetail = "presentMovieDetail"

    var complex: Row? = nil
    
    var moviesArray: [Row] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    fileprivate var selectedMovie: Row? = nil {
        didSet {
            if selectedMovie == nil {
                return
            }
            
            performSegue(withIdentifier: segueIdentifier_presentMovieDetail, sender: self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moviesArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "MovieTableViewCell", for: indexPath) as? MovieTableViewCell
            else {
                return UITableViewCell()
        }
        
        cell.fillData(movieTitle: moviesArray[indexPath.row][MovieHelper.title],
                      movieGenre: moviesArray[indexPath.row][MovieHelper.genre],
                      movieClassification: moviesArray[indexPath.row][MovieHelper.classification])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedMovie = moviesArray[indexPath.row]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let movieDetailPagerViewController = segue.destination as? MovieDetailPagerViewController,
            let identifier = segue.identifier, identifier == segueIdentifier_presentMovieDetail,
            let selectedMovie = selectedMovie,
            let complex = complex {
            
            movieDetailPagerViewController.movie = selectedMovie
            movieDetailPagerViewController.complex = complex
        }
    }
}

fileprivate extension MoviesTableViewController {
    func loadData(){
        guard let complex = complex else { return }
        
        moviesArray = SQLiteDataManager.shared.getMoviesAsArray(by: complex)
    }
}
