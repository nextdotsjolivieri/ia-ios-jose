//
//  MovieImageFromGalleryViewController.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 3/5/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import Foundation
import UIKit
import SQLite

class MovieImageFromGalleryViewController: UIViewController, UIScrollViewDelegate{
    
    @IBOutlet weak var imageSelected: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!{
        didSet {
            scrollView.minimumZoomScale = 1.0
            scrollView.maximumZoomScale = 6.0
            scrollView.delegate = self
        }
    }
    
    var multimediaArray: [Row] = []
    var selected : Int = 0
    fileprivate var isZoom : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if multimediaArray.count > 0{
            loadImage(ind: selected)
        }
        
        pageControl.numberOfPages = multimediaArray.count
        pageControl.currentPage = selected
        
        pageControl.addTarget(self, action: #selector(changedValuePageControl), for: UIControlEvents.valueChanged)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action:#selector(swipeWithGestureRecognizer))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right;
        
        let swipeLeft:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeWithGestureRecognizer))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left;
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        doubleTap.numberOfTapsRequired = 2
        view.addGestureRecognizer(doubleTap)
        
        imageSelected.isUserInteractionEnabled = true
        imageSelected.addGestureRecognizer(swipeRight)
        imageSelected.addGestureRecognizer(swipeLeft)
    }
    
    func changedValuePageControl(){
        selected = pageControl.currentPage
        loadImage(ind: selected)
    }

    func swipeWithGestureRecognizer(gesture:UISwipeGestureRecognizer){
        if gesture.direction == UISwipeGestureRecognizerDirection.right {
            if selected>0{
                selected-=1
                loadImage(ind: selected)
            }
        }
        else if gesture.direction == UISwipeGestureRecognizerDirection.left {
            if selected<multimediaArray.count-1{
                selected+=1
                loadImage(ind: selected)
            }
        }
        pageControl.currentPage = selected
    }
    
    func doubleTapped(){
        scrollView.zoomScale = isZoom ? 1.0 : 6.0
        isZoom = !isZoom
    }
    
    func loadImage(ind : Int) {
        guard let imageName = multimediaArray[ind][MultimediaHelper.file] else {
            return
        }
        let imageURL = URL(string: kImageBaseURL + imageName)
        imageSelected.kf.setImage(with: imageURL)
        isZoom = false
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView?{
        return imageSelected
    }
}
