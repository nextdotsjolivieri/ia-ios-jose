//
//  MovieGalleryCollectionViewController.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/3/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import UIKit
import SQLite
import XLPagerTabStrip

class MovieGalleryCollectionViewController: UICollectionViewController {
    
    fileprivate final let itemInfo: IndicatorInfo = "Gallery"
    
    fileprivate let numberOfItemsPerRow : Int = 3
    var delegate: MovieDetailTableViewControllerDelegate! = nil
    
    var movie: Row? = nil
    
    var multimediaArray: [Row] = [] {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    // MARK: UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return multimediaArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MultimediaImageCollectionViewCell", for: indexPath) as? MultimediaImageCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        cell.fillData(imageName: multimediaArray[indexPath.item][MultimediaHelper.file])
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("item \(indexPath)")
        delegate.showImagesFromGalley(images: multimediaArray, ind: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(numberOfItemsPerRow))
        return CGSize(width: size, height: size / 2)
    }

}

// MARK: - IndicatorInfoProvider

extension MovieGalleryCollectionViewController: IndicatorInfoProvider{
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

fileprivate extension MovieGalleryCollectionViewController {
    func loadData(){
        guard let movie = movie else { return }
        multimediaArray = SQLiteDataManager.shared.getMultimediaAsArray(by: movie)
        setup()
        
    }
    
    func setup(){
        let flow = self.collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        flow.sectionInset = UIEdgeInsetsMake(10, 10, 0, 10)
    }
}
