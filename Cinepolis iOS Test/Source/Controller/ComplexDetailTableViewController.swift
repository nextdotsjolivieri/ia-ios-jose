//
//  ComplexDetailTableViewController.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/2/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import UIKit
import SQLite

class ComplexDetailTableViewController: UITableViewController {
    
    private final let segueIdentifier_presentMovies = "presentMovies"
    private final let fieldsTitle = ["Name", "Phone", "Address", "Latitude", "Longitude"]
    
    var complex: Row? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 95
    }

    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldsTitle.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "FieldValueTableViewCell", for: indexPath) as? FieldValueTableViewCell,
            let complex = complex
            else {
                return UITableViewCell()
        }
        
        let field: String? = fieldsTitle[indexPath.row]
        var value: String? = ""
        
        switch indexPath.row {
        case 0:
            value = complex[ComplexHelper.name]
        case 1:
            value = complex[ComplexHelper.phone]
        case 2:
            value = complex[ComplexHelper.address]
        case 3:
            value = complex[ComplexHelper.latitude]
        default:
            value = complex[ComplexHelper.longitude]
        }
        
        cell.fillData(field: field, value: value)
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let moviesTableViewController = segue.destination as? MoviesTableViewController,
            let identifier = segue.identifier, identifier == segueIdentifier_presentMovies,
            let complex = complex {
            
            moviesTableViewController.complex = complex
        }
    }
    
    @IBAction func moviesButtonTapped(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: segueIdentifier_presentMovies, sender: self)
    }
}
