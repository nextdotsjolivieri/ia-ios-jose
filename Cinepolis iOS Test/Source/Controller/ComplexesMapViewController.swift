//
//  ComplexesMapViewController.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/2/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import UIKit
import MapKit
import SQLite

class ComplexesMapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    fileprivate final let regionRadius: CLLocationDistance = 2000
    fileprivate final let segueIdentifier_presentComplexDetail = "presentComplexDetail"
    
    fileprivate var selectedComplex: Row? = nil {
        didSet {
            if selectedComplex == nil {
                return
            }
            
            performSegue(withIdentifier: segueIdentifier_presentComplexDetail, sender: self)
        }
    }
    
    var city: City? = nil
    var complexesArray: [Row] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateMap()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let complexDetailTableViewController = segue.destination as? ComplexDetailTableViewController,
            let identifier = segue.identifier, identifier == segueIdentifier_presentComplexDetail,
            let selectedComplex = selectedComplex {
            
            complexDetailTableViewController.complex = selectedComplex
        }
    }
}

fileprivate extension ComplexesMapViewController {
    func updateMap(){
        guard let city = city else {
            return
        }
        
        let cityLocation = CLLocation(latitude: city.latitude, longitude: city.longitude)
        centerMapOnLocation(location: cityLocation)
        addComplexesPin()
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func addComplexesPin(){
        complexesArray.forEach { (complexRow) in
            if let latitudeString = complexRow[ComplexHelper.latitude],
                let latitude = Double(latitudeString),
                let longitudeString = complexRow[ComplexHelper.longitude],
                let longitude = Double(longitudeString) {
            
                let cityLocation = CLLocationCoordinate2DMake(latitude, longitude)
                let dropPin = ComplexPointAnnotation(complex: complexRow)
                dropPin.coordinate = cityLocation
                dropPin.title = complexRow[ComplexHelper.name]
                mapView.addAnnotation(dropPin)
            }
        }
        
        mapView.showAnnotations(mapView.annotations, animated: true)
    }
    
    @objc func calloutDidTappedAction(sender : UIButton) {
        guard let complexCallOutButton = sender as? ComplexCallOutButton,
            let complexPointAnnotation = complexCallOutButton.annotation else {
                return
        }
        
        selectedComplex = complexPointAnnotation.complex
    }
}

extension ComplexesMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? ComplexPointAnnotation,
            let complex = annotation.complex else {
            return nil
        }
        
        let callOutButton = ComplexCallOutButton(type: .detailDisclosure)
        callOutButton.annotation = annotation
        callOutButton.addTarget(self, action: #selector(calloutDidTappedAction), for: .touchUpInside)
        
        let identifier = "\(complex[ComplexHelper.id])"
        
        let view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        view.canShowCallout = true
        view.calloutOffset = CGPoint(x: -5, y: -5)
        view.rightCalloutAccessoryView = callOutButton
            
        return view
    }
}
