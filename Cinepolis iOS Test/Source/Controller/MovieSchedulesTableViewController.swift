//
//  MovieSchedulesTableViewController.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/3/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import UIKit
import SQLite
import XLPagerTabStrip
import Social

class MovieSchedulesTableViewController: UITableViewController {

    fileprivate final let itemInfo: IndicatorInfo = "Schedules"
    
    var billboardArray: [Row] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    var selectedBillboard: Row? = nil
    
    var movie: Row? = nil
    var complex: Row? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return billboardArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "MovieScheduleTableViewCell", for: indexPath) as? MovieScheduleTableViewCell
            else {
                return UITableViewCell()
        }
        
        cell.delegate = self
        cell.fillData(schedule: billboardArray[indexPath.row][BillboardHelper.schedule],
                      date: billboardArray[indexPath.row][BillboardHelper.date],
                      cinema: billboardArray[indexPath.row][BillboardHelper.cinema])
        
        return cell
    }
}

// MARK: - IndicatorInfoProvider

extension MovieSchedulesTableViewController: IndicatorInfoProvider{
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

fileprivate extension MovieSchedulesTableViewController {
    func loadData(){
        guard let movie = movie, let complex = complex else { return }
        billboardArray = SQLiteDataManager.shared.getBillboardAsArray(by: movie, and: complex)
    }
    
    func presentOptionsForSharing(){
        guard let _ = movie, let _ = selectedBillboard else { return }
        
        let alertController = UIAlertController(title: kShareTitle, message: kShareMessage, preferredStyle: .actionSheet)
        
        let twitterButton = UIAlertAction(title: kTwitter, style: .default, handler: { [unowned self] (action) -> Void in
            self.prepareForShare(with: SLServiceTypeTwitter)
        })
        
        let facebookButton = UIAlertAction(title: kFacebook, style: .default, handler: { [unowned self] (action) -> Void in
            self.prepareForShare(with: SLServiceTypeFacebook)
        })
        
        let cancelButton = UIAlertAction(title: kCancelTitle, style: .cancel, handler: nil)
        
        alertController.addAction(twitterButton)
        alertController.addAction(facebookButton)
        alertController.addAction(cancelButton)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func prepareForShare(with serviceType: String){
        guard let movie = movie, let billboard = selectedBillboard else { return }
        
        if SLComposeViewController.isAvailable(forServiceType: serviceType){
            if let vc = SLComposeViewController(forServiceType: serviceType) {
                var initialText = "Shedule for '\(movie[MovieHelper.title]!)'\n\n"
                initialText += BillboardHelper.toString(billboard: billboard)
                vc.setInitialText(initialText)
                present(vc, animated: true)
            }
        } else {
            let alert = UIAlertController(title: kShareTitle, message: kLoginToShareTitle, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: kOkTitle, style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension MovieSchedulesTableViewController: MovieScheduleTableViewCellDelegate {
    func didSelectShare(cell: MovieScheduleTableViewCell){
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        selectedBillboard = billboardArray[indexPath.row]
        presentOptionsForSharing()
    }
}
