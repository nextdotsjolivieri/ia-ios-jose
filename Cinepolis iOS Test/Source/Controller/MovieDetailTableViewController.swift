//
//  MovieDetailTableViewController.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/3/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import UIKit
import SQLite
import XLPagerTabStrip

class MovieDetailTableViewController: UITableViewController {

    fileprivate final let itemInfo: IndicatorInfo = "Info"
    private final let fieldsTitle = ["Title", "Synopsis", "Genre", "Classification", "Duration", "Director", "Actors"]

    
    var movie: Row? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 95
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fieldsTitle.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "FieldValueTableViewCell", for: indexPath) as? FieldValueTableViewCell,
            let movie = movie
            else {
                return UITableViewCell()
        }
        
        let field: String? = fieldsTitle[indexPath.row]
        var value: String? = ""
        
        switch indexPath.row {
        case 0:
            value = movie[MovieHelper.title]
        case 1:
            value = movie[MovieHelper.synopsis]
        case 2:
            value = movie[MovieHelper.genre]
        case 3:
            value = movie[MovieHelper.classification]
        case 4:
            value = movie[MovieHelper.duration]
        case 5:
            value = movie[MovieHelper.director]
        default:
            value = movie[MovieHelper.actors]
        }
        
        cell.fillData(field: field, value: value)
        
        return cell
    }
}

// MARK: - IndicatorInfoProvider

extension MovieDetailTableViewController: IndicatorInfoProvider{
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

