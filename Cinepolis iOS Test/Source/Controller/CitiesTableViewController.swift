//
//  CitiesTableViewController.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/1/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import UIKit
import RealmSwift
import SVProgressHUD

class CitiesTableViewController: UITableViewController {
    
    private final let segueIdentifier_presentComplexes = "presentComplexes"
    
    fileprivate var cities : [City]? = nil {
        didSet {
            tableView.reloadData()
        }
    }
    
    fileprivate var selectedDatabase: CinePolisDatabase? = nil {
        didSet {
            if selectedDatabase == nil {
                return
            }
            
            SQLiteDataManager.shared.database = selectedDatabase
            performSegue(withIdentifier: segueIdentifier_presentComplexes, sender: self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "CityTableViewCell", for: indexPath) as? CityTableViewCell,
            let city = cities?[indexPath.row]
            else {
                return UITableViewCell()
        }

        cell.fillData(cityName: city.name)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let city = cities?[indexPath.row] else { return }
        getInfoFromCity(city: city)
    }
}

fileprivate extension CitiesTableViewController {
    
    func loadData(){
        fetchRemoteCities()
        fetchLocalCities()
    }
    
    func fetchRemoteCities() {
        SVProgressHUD.show(withStatus: kLoadingTitle)
        DataManager.fetchData(url: kCitiesURL, type: City.self, success: { [unowned self] (cities) in
            SVProgressHUD.dismiss()
            self.cities = cities
            self.saveCities(cities)
        }) { (error) in
            print(error)
        }
    }
    
    func fetchLocalCities() {
        do {
            let realm = try Realm()
            cities = Array(realm.objects(City.self))
        } catch let error as NSError {
            print(error)
        }
    }
    
    func fetchDatabase(from city: City) -> Bool{
        do {
            let realm = try Realm()
            let predicate = NSPredicate(format: "city = %@", city)
            selectedDatabase = realm.objects(CinePolisDatabase.self).filter(predicate).first
        } catch let error as NSError {
            print(error)
            return false
        }
        
        return selectedDatabase != nil
    }
    
    func saveCities(_ cities: [City]) {
        do {
            let realm = try Realm()
            try realm.write {
                cities.forEach({ (city) in
                    realm.add(city, update: true)
                })
            }
        } catch let error as NSError {
            print(error)
        }
    }
    
    func saveDatabase(databasePath: String, `for` city: City){
        do {
            let database = CinePolisDatabase(filePath: databasePath, city: city)
            let realm = try Realm()
            try realm.write {
                realm.add(database, update: true)
            }
            selectedDatabase = database
        } catch let error as NSError {
            print(error)
        }
    }
    
    func getInfoFromCity(city: City){
        if !fetchDatabase(from: city) {
            SVProgressHUD.show(withStatus: kDownloadingTitle)            
            let urlFile = kDatabaseURL + String(city.id)
            DataManager.fetchFile(showProgress: true, urlFile: urlFile, success: { [unowned self] (databaseURL) in
                SVProgressHUD.dismiss()
                self.saveDatabase(databasePath: databaseURL.lastPathComponent, for: city)
            })
        }
    }
}
