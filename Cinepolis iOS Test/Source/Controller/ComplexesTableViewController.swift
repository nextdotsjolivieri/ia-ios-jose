//
//  ComplexesTableViewController.swift
//  Cinepolis iOS Test
//
//  Created by José Valderrama on 5/1/17.
//  Copyright © 2017 José Valderrama. All rights reserved.
//

import UIKit
import SQLite

class ComplexesTableViewController: UITableViewController {
    
    private final let segueIdentifier_presentMovies = "presentMovies"
    private final let segueIdentifier_presentComplexesMap = "presentComplexesMap"
    
    var complexesArray: [Row] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    fileprivate var selectedComplex: Row? = nil {
        didSet {
            if selectedComplex == nil {
                return
            }
            
            performSegue(withIdentifier: segueIdentifier_presentMovies, sender: self)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 95
        loadData()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return complexesArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: "FieldValueTableViewCell", for: indexPath) as? FieldValueTableViewCell
            else {
                return UITableViewCell()
        }
        
        cell.fillData(field: complexesArray[indexPath.row][ComplexHelper.name], value: complexesArray[indexPath.row][ComplexHelper.address])
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedComplex = complexesArray[indexPath.row]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let moviesTableViewController = segue.destination as? MoviesTableViewController,
            let identifier = segue.identifier, identifier == segueIdentifier_presentMovies,
            let selectedComplex = selectedComplex {
            
            moviesTableViewController.complex = selectedComplex
        } else if let complexesMapViewController = segue.destination as? ComplexesMapViewController,
                let identifier = segue.identifier, identifier == segueIdentifier_presentComplexesMap,
                let database = SQLiteDataManager.shared.database {
            
            complexesMapViewController.city = database.city
            complexesMapViewController.complexesArray = complexesArray
        }
    }
    
    @IBAction func mapButtonTapped(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: segueIdentifier_presentComplexesMap, sender: self)
    }
}

fileprivate extension ComplexesTableViewController {
    func loadData(){
        complexesArray = SQLiteDataManager.shared.getComplexesAsArray()
    }
}
