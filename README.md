# IA Interactive iOS Test

This project shows how to use some services of CinePolis

## Installation

1. Clone
2. Install [Cocoapods](https://cocoapods.org)
3. Run `pod install` at the same folder level as the `podfile` file
4. Run the app

## Third-party

1. `SVProgressHUD` (https://github.com/SVProgressHUD/SVProgressHUD)
2. `Kingfisher` (https://github.com/onevcat/Kingfisher)
3. `AlamofireObjectMapper` (https://github.com/tristanhimmelman/AlamofireObjectMapper)
4. `RealmSwift` (https://realm.io/docs/swift/latest)
5. `SQLite.swift` (https://github.com/stephencelis/SQLite.swift)
6. `XLPagerTabStrip` (https://github.com/xmartlabs/XLPagerTabStrip)